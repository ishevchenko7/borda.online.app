module.exports = {
    mode: 'production',
    optimization: {
        concatenateModules: false,
        minimize: true
    }
}
