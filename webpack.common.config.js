const webpack = require('webpack')
const { resolve } = require('path')

module.exports = {
    entry: './src/index.js',
    target: 'electron-renderer',
    output: { path: __dirname, filename: 'bundle.js' },
    externals: {
        sharp: 'commonjs sharp',
        sqlite3: 'commonjs sqlite3',
        knex: 'commonjs knex',
        archiver: "require('archiver')",
    },

    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /node_modules[\/\\](iconv-lite)[\/\\].+/,
                resolve: {
                    aliasFields: ['main'],
                },
            },
        ],
    },

    resolve: {
        alias: {
            '#tools': resolve(__dirname, 'src/tools/'),
            '#actions': resolve(__dirname, 'src/actions/'),
            '#db': resolve(__dirname, 'src/db/'),
            '#remote': resolve(__dirname, 'src/remote/'),
            '#common': resolve(__dirname, 'src/common/'),
            '#views': resolve(__dirname, 'src/views/'),
            '#logger': resolve(__dirname, 'src/logger/'),
        },
    },
}
