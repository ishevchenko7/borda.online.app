'use strict'
const electron = require('electron')
const path = require('path')
const isDev = require('electron-is-dev')
const { autoUpdater } = require('electron-updater')
const log = require('electron-log')

const app = electron.app
app.commandLine.appendSwitch('js-flags', '--harmony-async-iteration')

autoUpdater.logger = log
autoUpdater.logger.transports.file.level = 'info'

// Prevent window being garbage collected
let mainWindow

function onClosed() {
    // Dereference the window
    // For multiple windows store them in an array
    mainWindow = null
}

function createMainWindow() {
    const appName = require('./package.json').name
    const appVersion = require('./package.json').version

    const mainWindow = new electron.BrowserWindow({
        width: 500,
        height: 400,
        webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true,
        },
        icon: path.join(__dirname, 'icon.png'),
    })

    if (isDev) {
        mainWindow.setSize(1000, 600)
        mainWindow.webContents.openDevTools()
    } else {
        mainWindow.removeMenu()
    }

    mainWindow.loadURL(`file://${__dirname}/index.html`)
    mainWindow.on('closed', onClosed)

    mainWindow.webContents.on('did-finish-load', () => {
        mainWindow.setTitle(`${appName} - v${appVersion}`)
    })

    return mainWindow
}

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function () {
    if (!mainWindow) {
        mainWindow = createMainWindow()
    }
})

app.on('ready', function () {
    mainWindow = createMainWindow()

    if (!isDev) {
        autoUpdater.checkForUpdates()
    }
})

autoUpdater.on('update-downloaded', async function (info) {
    const dialogOpts = {
        type: 'info',
        buttons: ['Restart'],
        title: 'Application update',
        detail:
            'A new version has been downloaded. Please restart the application to apply the updates',
    }

    await electron.dialog.showMessageBox(dialogOpts)
    autoUpdater.quitAndInstall()
})
