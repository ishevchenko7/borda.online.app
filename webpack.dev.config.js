const webpack = require('webpack')

module.exports = {
    mode: 'development',
    devtool: false,
    plugins: [new webpack.SourceMapDevToolPlugin({})],
}
