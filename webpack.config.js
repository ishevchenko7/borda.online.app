const { merge } = require('webpack-merge')
const commonConfig = require('./webpack.common.config')

module.exports = ({ env }) => {
    const envConfig = require(`./webpack.${env}.config.js`);
    return merge(commonConfig, envConfig);
}