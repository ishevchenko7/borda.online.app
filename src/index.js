import * as settingsStore from '#common/settings-store'
import interaction from './views/login/interaction'
import prepareDb from '#db/prepare'

settingsStore.set({
    reconnectDelay: 5000,
})

async function start() {
    await prepareDb()
    interaction.start()
}

start()
