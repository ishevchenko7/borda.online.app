import html from 'html-loader!./index.html'
import styles from 'css-loader!./styles.css'

const styleSheet = document.createElement("style")
styleSheet.innerText = styles
document.head.appendChild(styleSheet)
document.body.innerHTML = html

export default class LoginDialogs {
    constructor() {
        const dialogsContent = document.querySelector('#login_dialogs')
        document.body.appendChild(dialogsContent.cloneNode(true))

        this.dialogs = {}

        this.createLoginDialog()
        this.createConnectingDialog()
        this.createAdminDialog()
        this.createMessageDialog()
        this.createConfirmDialog()
        this.createOnlineDialog()
    }

    createLoginDialog() {
        const node = document.querySelector('#login_dialog')
        this.dialogs.login = {
            node,
            fields: {
                login: node.querySelector('#login'),
                password: document.querySelector('#login_password'),
            },
        }

        node.querySelector('form').addEventListener('submit', () =>
            this.dialogs.login.closeEventResolver()
        )
    }

    createConnectingDialog() {
        this.dialogs.connecting = {
            node: document.querySelector('#connecting_dialog'),
        }
    }

    createAdminDialog() {
        const node = document.querySelector('#admin_dialog')
        this.dialogs.admin = {
            node,
            fields: {
                password: node.querySelector('#password'),
                confirm: document.querySelector('#confirm'),
            },
        }

        node.querySelector('form').addEventListener('submit', () => {
            const fields = this.getFields('admin')
            this.dialogs.admin.closeEventResolver(fields)
        })
    }

    createMessageDialog() {
        const node = document.querySelector('#message_dialog')
        this.dialogs.message = {
            node,
        }

        node.querySelector('form').addEventListener('submit', () =>
            this.dialogs.message.closeEventResolver()
        )
    }

    createConfirmDialog() {
        const node = document.querySelector('#confirm_dialog')
        this.dialogs.confirm = {
            node,
        }

        node.querySelectorAll('button').forEach((button, number) =>
            button.addEventListener('click', () => {
                this.dialogs.confirm.closeEventResolver(number)
            })
        )
    }

    createOnlineDialog() {
        this.dialogs.online = { node: document.querySelector('#online_dialog') }
    }

    getFields(name) {
        return _.mapValues(this.dialogs[name].fields, ({ value }) => value)
    }

    closeAllDialogs() {
        _.forOwn(this.dialogs, ({ node }, key) => {
            node.close()
        })
    }

    resetFields(node) {
        const form = node.querySelector('form')
        if (form) form.reset()
    }

    async show(name, params = {}) {
        const dialog = this.dialogs[name]
        const { node } = dialog

        this.closeAllDialogs()

        switch (name) {
            case 'login':
                this.resetFields(node)
                node.querySelector('#login').value = params.login || ''
                break

            case 'admin':
                this.resetFields(node)
                break

            case 'confirm':
                node.querySelector('p').innerHTML = params.question
                break

            case 'message':
                const { error, message } = params
                node.querySelector('form > div > p').innerHTML =
                    message || error
                break

            case 'online':
            case 'connecting':
                node.showModal()
                return
        }

        node.showModal()

        if (name === 'login' && params.login) {
            node.querySelector('#login_password').focus()
        }

        return await new Promise((resolve, reject) => {
            dialog.closeEventResolver = resolve
        })
    }
}
