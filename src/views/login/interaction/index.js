import bcrypt from 'bcryptjs'

import db from '#db'
import LoginDialog from '../dialogs'
import { connect } from '#remote'

class LoginInteraction extends LoginDialog {
    constructor() {
        super()
    }

    checkPassword({ password, confirm }) {
        if (!password) {
            return 'Password not specified'
        }

        if (password !== confirm) {
            return 'Password and confirm does not match'
        }
    }

    async eventNotify(event) {
        switch (event.type) {
            case 'initialized':
                const admin = Boolean(await db.user.get('admin'))

                if (!admin) {
                    let { password } = this.getFields('login')

                    if (
                        !(await this.show('confirm', {
                            question: 'Create admin user with this password?',
                        }))
                    ) {
                        let error

                        do {
                            let confirm
                            ;({ password, confirm } = await this.show('admin'))

                            error = this.checkPassword({
                                password,
                                confirm,
                            })
                            if (error) {
                                await this.show('message', { error })
                            }
                        } while (error)
                    }

                    const hashed_password = await bcrypt.hash(password, 8)
                    await db.user.add({
                        name: 'admin',
                        full_name: 'Administrator',
                        hashed_password,
                    })
                }

                this.show('online')

                break
            case 'login_fail':
                await this.show('message', { error: event.failMessage })
                this.login()
                break
        }
    }

    async login() {
        const board = await db.board.get()
        await this.show('login', { login: board?.code })

        connect()
    }

    start() {
        this.login()
    }
}

export default new LoginInteraction()
