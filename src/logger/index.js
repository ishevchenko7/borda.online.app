import pino from 'pino'

export default pino({
    prettyPrint: { colorize: true },
    browser: { asObject: true },
    timestamp: false,
    base: null,
})