import { format } from 'url'

import logger from '#logger'
import interaction from '#views/login/interaction'
import * as settingsStore from '#common/settings-store'
import ServiceActionsDispatcher from '#actions/service'
import { SocketTransceiver } from '#common/signal-ready'

let socket
let connectTimer

function connect(delay) {
    clearTimeout(connectTimer)

    connectTimer = setTimeout(function () {
        interaction.show('connecting')

        const url = format({
            protocol: 'wss:',
            hostname: 'borda.online',
            pathname: '/ws/',
            query: { source: 'app' },
        })

        socket = new WebSocket(url)
        const serviceActionsDispatcher = new ServiceActionsDispatcher({
            transceiver: new SocketTransceiver({ socket }),
        })

        socket.onclose = (event) => {
            logger.info({ event }, 'WebSocket close event')

            const { code } = event
            switch (code) {
                // Forbidden or internal error
                case 4403:
                case 4500:
                    logger.info('Stop webSocket reconnect')

                    // Internal error
                    if (code === 4500) {
                        logger.error('Service internal error')
                        interaction.login()
                    }

                    break
                default:
                    connect(settingsStore.get().reconnectDelay)
            }
        }

        socket.onopen = (event) => {
            logger.info({ event }, 'WebSocket open event')
        }

        socket.onerror = (event) => {
            logger.info({ event }, 'WebSocket error event')
            connect(settingsStore.get().reconnectDelay)
        }
    }, delay)
}

export { connect }
