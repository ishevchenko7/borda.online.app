import { ActionsDispatchersStore } from '#common/signal-ready'
import SiteActionsDispatcher from '#actions/site'

class SiteActionsDispatchersStore extends ActionsDispatchersStore {
    constructor() {
        super({ closeMarkFlag: true })
    }

    add(id, params) {
        const dispatcher = new SiteActionsDispatcher(params)
        super.add(id, dispatcher)
    }
}

export default new SiteActionsDispatchersStore()
