import db from '#db'
import interaction from '#views/login/interaction'
import * as settingsStore from '#common/settings-store'

export default async function (args) {
    const { board, settings } = args

    const isNoBoard = !Boolean(await db.board.get())
    if (isNoBoard) {
        await db.board.add(_.pick(board, ['code', 'name']))
    }
    settingsStore.set(settings)

    interaction.eventNotify({ type: 'initialized' })
}
