import _ from 'lodash'

import { RtcTransceiver } from '#common/signal-ready'
import * as settingsStore from '#common/settings-store'
import siteActionsDispatchersStore from '#remote/site_actions_dispatchers_store'

export default async function (args, dispatcher) {
    const { user, settings } = args

    if (!siteActionsDispatchersStore.get({ user })) {
        const settingsPath = user
        if (settings) {
            settingsStore.set(settings, settingsPath)
        } else {
            settingsStore.copy('connection', settingsPath)
        }

        const transceiver = new RtcTransceiver({
            user,
            settingsPath,
            signalSender: async (signal) =>
                await dispatcher.remoteCall({
                    type: 'signal',
                    args: { user, signal },
                }),
        })

        transceiver.addEventListener('close', () => {
            settingsStore.remove(settingsPath)
        })

        siteActionsDispatchersStore.add(user, {
            transceiver,
            settingsPath: `${settingsPath}.actionsDispatcher`,
        })
    }
}
