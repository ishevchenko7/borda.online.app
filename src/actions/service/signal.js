import { signalHandler } from '#common/signal-ready'
import siteActionsDispatchersStore from '#remote/site_actions_dispatchers_store'

export default async function (args) {
    const { user, signal } = args

    const siteActionsDispatcher = siteActionsDispatchersStore.get({ id: user })
    if (siteActionsDispatcher) {
        const { transceiver } = siteActionsDispatcher
        await signalHandler({ transceiver, signal })
    } else {
        return { error: 'Unknown user' }
    }
}
