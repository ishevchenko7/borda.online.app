import interaction from '#views/login/interaction'

export default async function () {
    return interaction.getFields('login')
}
