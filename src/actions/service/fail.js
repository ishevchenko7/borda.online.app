import interaction from '#views/login/interaction'

export default async function (args) {
    const { failMessage } = args

    interaction.eventNotify({
        type: 'login_fail',
        failMessage,
    })
}
