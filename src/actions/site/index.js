import _ from 'lodash'
import { ActionsDispatcher } from '#common/signal-ready'
import ArchiversStore from '#tools/disk/archivers_store'

const actions = require.context('./', true, /^\.\/(?!index(\.js)?$)/)
const ADMIN_ACTION_TYPES = [/^apps\/admin-panel\/.*$/, 'apps/components/share']

class SiteActionsDispatcher extends ActionsDispatcher {
    constructor(params) {
        super({ ...params, actions, archiversStore: new ArchiversStore() })
    }

    get isAdmin() {
        return this.currentUser?.name === 'admin'
    }

    get isAnonymous() {
        return !Boolean(this.currentUser)
    }

    checkPermission(action) {
        const isAdminAction = Boolean(
            _.find(ADMIN_ACTION_TYPES, (type) =>
                new RegExp(type).test(action.type)
            )
        )

        if (isAdminAction) {
            return this.isAdmin
        }

        return true
    }
}

export default SiteActionsDispatcher
