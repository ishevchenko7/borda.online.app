import _ from 'lodash'
import path from 'path'
import sharp from 'sharp'

import { replaceAlias } from '#tools/disk'

const options = {
    entropy: {
        fit: 'cover',
        withoutEnlargement: true,
        position: sharp.strategy.entropy,
    },

    outside: {
        fit: 'outside',
        withoutEnlargement: true,
        background: { r: 255, g: 255, b: 255, alpha: 0 },
    },
}

export default async function (args, dispatcher) {
    const { files, width, height, optionPreset } = args
    const { isAdmin, isAnonymous, currentUrlPath } = dispatcher

    let warn
    const thumbnails = []
    for (const file of files) {
        let name
        let dir = args.dir || ''

        if (_.isString(file)) {
            name = file
        } else {            
            name = file.name
            dir = dir || file.dir ? path.join(dir, file.dir) : ''
        }

        const fullName = path.format({
            dir,
            base: name,
        })

        const directFullName = isAdmin
            ? fullName
            : await replaceAlias({
                  fullName,
                  currentUrlPath,
                  anonymous: isAnonymous,
              })

        let errorCounter = 0
        try {
            const { data } = await sharp(directFullName, { failOnError: false })
                .rotate()
                .resize({
                    width,
                    height,
                    ...options[optionPreset],
                })
                .toBuffer({ resolveWithObject: true })

            thumbnails.push(data)
        } catch (error) {
            errorCounter++
            warn = error.code || error.message

            if (errorCounter === files.length) {
                throw error
            }

            thumbnails.push(null)
        }
    }

    return { thumbnails, warn }
}
