import db from '#db'

export default async function (args, dispatcher) {
    const { video } = args
    let { deviceId } = video
    const { transceiver, isAnonymous, isAdmin, currentUrlPath } = dispatcher

    if (!isAdmin) {
        const shared = await db.shared
            .getUrlDisabled({
                type: 'webcam',
                alias: deviceId,
                currentUrlPath,
                anonymous: isAnonymous,
            }).first()

        if (shared?.name) {
            deviceId = shared.name
        } else {
            throw Error('Wrong webcam alias')
        }
    }

    const streamId = await transceiver.addStream({
        video: { ...video, deviceId },
    })

    return { streamId }
}
