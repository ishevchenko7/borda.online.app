export default async function (args, dispatcher) {
    const { trackId } = args
    const { transceiver } = dispatcher

    transceiver.removeTrack(trackId)
}
