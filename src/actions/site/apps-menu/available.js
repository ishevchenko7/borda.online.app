import _ from 'lodash'
import * as settingsStore from '#common/settings-store'

export default async function (args, { isAdmin }) {
    const { apps } = settingsStore.get()

    const available = []
    _.forOwn(apps, ({ admin }, name) => {
        if ((admin && isAdmin) || !admin) {
            available.unshift(name)
        }
    })

    return {
        apps: _.orderBy(available),
    }
}
