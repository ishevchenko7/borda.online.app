import _ from 'lodash'
import db from '#db'

const sharedTypeRelatedApps = {
    dir: ['audio', 'disk', 'gallery'],
    file: ['audio', 'disk', 'gallery'],
    webcam: ['cctv'],
}

export default async function (args, { isAnonymous, isAdmin }) {
    if (!isAdmin) {
        const { name } = args
        const types = _.reduce(
            sharedTypeRelatedApps,
            (result, types, key) => {
                if (_.includes(types, name)) {
                    result.push(key)
                }
                return result
            },
            []
        )

        if (!_.isEmpty(types)) {
            const shared = await db.shared.get({
                types,
                disabled: false,
                url_path: null,
                anonymous: isAnonymous,
            })

            if (_.isEmpty(shared)) {
                return {
                    hasNoContent: true,
                    message: isAnonymous
                        ? 'Application content not available for anonymous user. \n\rPlease login.'
                        : 'Application content available only for admin user.',
                }
            }
        }
    }
}
