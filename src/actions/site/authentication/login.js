import bcrypt from 'bcryptjs'
import db from '#db'

export default async function (args, dispatcher) {
    const { login, password } = args
    const board = await db.board.get()

    if (board) {
        let authenticated = false
        const user = login && (await db.user.get(login))

        if (user) {
            const { blocked, hashed_password, full_name } = user
            authenticated =
                password &&
                !blocked &&
                hashed_password &&
                (await bcrypt.compare(password, hashed_password))

            if (authenticated) {
                dispatcher.currentUser = user
                return { authenticated, full_name }
            }
        }
    } else {
        return { error: { message: 'Board not initialized' } }
    }
}
