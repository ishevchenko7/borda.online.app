import siteActionsDispatchersStore from '#remote/site_actions_dispatchers_store'

export default async function (args, dispatcher) {
    const { dispatcherId } = args
    const source = siteActionsDispatchersStore.get({
        id: dispatcherId,
    })

    if (source) {
        _.assign(
            dispatcher,
            _.pick(source, ['currentUser', 'currentUrlPath', 'archiversStore'])
        )
    }

    return { authenticated: Boolean(dispatcher.currentUser) }
}
