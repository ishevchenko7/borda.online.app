import db from '#db'

export default async function (args) {
    const { id, value } = args
    return await db.shared.update({ id, value })
}
