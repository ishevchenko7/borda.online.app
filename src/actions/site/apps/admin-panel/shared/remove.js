import db from '#db'

export default async function (args) {
    const { id } = args
    return await db.shared.remove({ id })
}
