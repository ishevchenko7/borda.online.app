import { join } from 'path'
import db from '#db'

export default async function (args) {
    const { items } = args

    const shared = _.map(items, ({ path = '', name, ...other }) => ({
        name: join(path, name),
        ...other,
    }))

    await db.shared.add(shared)
}
