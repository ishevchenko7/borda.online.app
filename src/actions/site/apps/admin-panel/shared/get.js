import db from '#db'

export default async function (args) {
    const { types } = args
    return await db.shared.get({ types })
}
