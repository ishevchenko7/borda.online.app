import _ from 'lodash'
import db from '#db'

export default async function () {
    return await db.user.getPartial()
}
