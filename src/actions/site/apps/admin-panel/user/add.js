import db from '#db'

export default async function (args) {
    const { name, full_name } = args
    await db.user.add({ name, full_name })
}
