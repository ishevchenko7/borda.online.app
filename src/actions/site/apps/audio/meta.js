import _ from 'lodash'
import path from 'path'
import sharp from 'sharp'
import { parseFile } from 'music-metadata'

import { replaceAlias } from '#tools/disk'

const defaultMetaMap = {
    title: 'common.title',
    album: 'common.album',
    artist: 'common.artist',
    duration: 'format.duration',
}

const maxWidth = 200
const maxHeight = 200

export default async function (args, dispatcher) {
    const {
        file: { dir = '', name },
        metaMap,
        onlyPictures = false,
    } = args
    const { isAdmin, isAnonymous, currentUrlPath } = dispatcher

    let fullName = path.join(dir, name)
    fullName = isAdmin
        ? fullName
        : await replaceAlias({
              fullName,
              currentUrlPath,
              anonymous: isAnonymous,
          })

    const fullMeta = await parseFile(fullName)

    let meta
    if (onlyPictures) {
        const { picture } = fullMeta.common

        if (picture.length > 0) {
            const {
                data,
                info: { width, height },
            } = await sharp(picture[0].data)
                .resize(maxWidth, maxHeight, {
                    fit: sharp.fit.inside,
                    withoutEnlargement: true,
                })
                .toFormat('jpeg')
                .toBuffer({ resolveWithObject: true })

            return { picture: { sizes: `${width}x${height}`, data } }
        }
    } else {
        meta = _.reduce(
            metaMap || defaultMetaMap,
            function (result, value, key) {
                result[key] = _.get(fullMeta, value)
                return result
            },
            {}
        )

        meta.picturesCount = fullMeta.common.picture?.length || 0
    }

    return { meta }
}
