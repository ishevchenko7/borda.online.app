import _ from 'lodash'
import path from 'path'
import fs from 'fs'
import { once } from 'events'
import isStreamEnded from 'is-stream-ended'

import { replaceAlias } from '#tools/disk'

// TODO const!
const maxStepLength = 1024 * 120
const cacheLength = 1024 * 512

function skipId3(buffer) {
    let id3v2Flags, z0, z1, z2, z3, tagSize, footerSize

    //http://id3.org/d3v2.3.0
    if (buffer[0] === 0x49 && buffer[1] === 0x44 && buffer[2] === 0x33) {
        //'ID3'
        id3v2Flags = buffer[5]
        footerSize = id3v2Flags & 0x10 ? 10 : 0

        //ID3 size encoding is crazy (7 bits in each of 4 bytes)
        z0 = buffer[6]
        z1 = buffer[7]
        z2 = buffer[8]
        z3 = buffer[9]
        if (
            (z0 & 0x80) === 0 &&
            (z1 & 0x80) === 0 &&
            (z2 & 0x80) === 0 &&
            (z3 & 0x80) === 0
        ) {
            tagSize =
                (z0 & 0x7f) * 2097152 +
                (z1 & 0x7f) * 16384 +
                (z2 & 0x7f) * 128 +
                (z3 & 0x7f)
            return 10 + tagSize + footerSize
        }
    }
    return 0
}

let versions = ['2.5', 'x', '2', '1'],
    layers = ['x', '3', '2', '1'],
    bitRates = {
        V1Lx: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        V1L1: [
            0,
            32,
            64,
            96,
            128,
            160,
            192,
            224,
            256,
            288,
            320,
            352,
            384,
            416,
            448,
        ],
        V1L2: [
            0,
            32,
            48,
            56,
            64,
            80,
            96,
            112,
            128,
            160,
            192,
            224,
            256,
            320,
            384,
        ],
        V1L3: [
            0,
            32,
            40,
            48,
            56,
            64,
            80,
            96,
            112,
            128,
            160,
            192,
            224,
            256,
            320,
        ],
        V2Lx: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        V2L1: [
            0,
            32,
            48,
            56,
            64,
            80,
            96,
            112,
            128,
            144,
            160,
            176,
            192,
            224,
            256,
        ],
        V2L2: [0, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160],
        V2L3: [0, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160],
        VxLx: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        VxL1: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        VxL2: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        VxL3: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    },
    sampleRates = {
        x: [0, 0, 0],
        1: [44100, 48000, 32000],
        2: [22050, 24000, 16000],
        2.5: [11025, 12000, 8000],
    },
    samples = {
        x: {
            x: 0,
            1: 0,
            2: 0,
            3: 0,
        },
        1: {
            //MPEGv1,     Layers 1,2,3
            x: 0,
            1: 384,
            2: 1152,
            3: 1152,
        },
        2: {
            //MPEGv2/2.5, Layers 1,2,3
            x: 0,
            1: 384,
            2: 1152,
            3: 576,
        },
    }

function frameSize(samples, layer, bitRate, sampleRate, paddingBit) {
    if (layer === 1) {
        return ((samples * bitRate * 125) / sampleRate + paddingBit * 4) | 0
    } else {
        //layer 2, 3
        return ((samples * bitRate * 125) / sampleRate + paddingBit) | 0
    }
}

function parseFrameHeader(header) {
    let b1,
        b2,
        versionBits,
        version,
        simpleVersion,
        layerBits,
        layer,
        bitRateKey,
        bitRateIndex,
        bitRate,
        sampleRateIdx,
        sampleRate,
        paddingBit,
        sample

    b1 = header[1]
    b2 = header[2]

    versionBits = (b1 & 0x18) >> 3
    version = versions[versionBits]
    simpleVersion = version === '2.5' ? 2 : version

    layerBits = (b1 & 0x06) >> 1
    layer = layers[layerBits]

    bitRateKey = 'V' + simpleVersion + 'L' + layer
    bitRateIndex = (b2 & 0xf0) >> 4
    bitRate = bitRates[bitRateKey][bitRateIndex] || 0

    sampleRateIdx = (b2 & 0x0c) >> 2
    sampleRate = sampleRates[version][sampleRateIdx] || 0

    sample = samples[simpleVersion][layer]

    paddingBit = (b2 & 0x02) >> 1
    return {
        bitRate: bitRate,
        sampleRate: sampleRate,
        frameSize: frameSize(sample, layer, bitRate, sampleRate, paddingBit),
        samples: sample,
    }
}

async function read(stream, length) {
    let buffer
    while (length > 0 && stream.readable && !isStreamEnded(stream)) {
        const chunk = await stream.read(Math.min(length, stream.readableLength))

        if (chunk) {
            length -= chunk.length
            buffer = buffer ? Buffer.concat([buffer, chunk]) : chunk
        } else {
            if (!isStreamEnded(stream)) {
                await once(stream, 'readable')
            }
        }
    }

    return buffer
}

export default async function (args, dispatcher) {
    let {
        file: { dir, name },
    } = args
    const { isAdmin, isAnonymous, currentUrlPath } = dispatcher

    let fullName = path.format({
        dir,
        base: name,
    })
    fullName = isAdmin
        ? fullName
        : await replaceAlias({
              fullName,
              currentUrlPath,
              anonymous: isAnonymous,
          })

    let cache
    async function readCached(stream, length, keepBuffer = false) {
        if (!cache) {
            cache = await read(stream, cacheLength)
        }

        if (cache) {
            let result

            if (cache.length >= length) {
                result = cache.slice(0, length)
                if (!keepBuffer) {
                    cache =
                        cache.length === length
                            ? undefined
                            : cache.slice(length)
                }
            } else {
                const chunk = await read(stream, length - cache.length)
                if (chunk) {
                    result = Buffer.concat([cache, chunk])

                    if (keepBuffer) {
                        cache = result
                    } else {
                        cache = undefined
                    }
                }
            }

            return result
        }
    }

    const stream = fs.createReadStream(fullName, {
        highWaterMark: cacheLength * 2,
    })

    try {
        let buffer = await readCached(stream, 10, true)

        let duration = 0
        let frameSizes = []
        let corruptedFlag = false
        let offset = skipId3(buffer)
        await readCached(stream, offset)

        while (true) {
            buffer = await readCached(stream, 3, true)

            if (buffer?.length === 3) {
                if (buffer[0] === 0xff && (buffer[1] & 0xe0) === 0xe0) {
                    const info = parseFrameHeader(buffer)
                    if (info.frameSize && info.samples) {
                        if (corruptedFlag) {
                            if (frameSizes.length > 0) {
                                ;[duration] = frameSizes.pop()
                            }

                            corruptedFlag = false
                        }

                        frameSizes.push([duration, offset])
                        offset += info.frameSize
                        duration += info.samples / info.sampleRate

                        const frame = await readCached(stream, info.frameSize)
                        if (frame?.length != info.frameSize) {
                            break
                        }
                    } else {
                        offset++
                        corruptedFlag = true
                        await readCached(stream, 1)
                    }
                } else {
                    if (
                        buffer[0] === 0x54 &&
                        buffer[1] === 0x41 &&
                        buffer[2] === 0x47
                    ) {
                        await readCached(stream, 128)
                    } else {
                        offset++
                        corruptedFlag = true
                        await readCached(stream, 1)
                    }
                }
            } else {
                break
            }
        }

        const maxStepsCount = Math.max(1, Math.floor(offset / maxStepLength))
        const step = Math.ceil(frameSizes.length / maxStepsCount)
        const steps = _.filter(frameSizes, (value, index) => !(index % step))

        //Add offset for calculate last step size
        steps.push([duration, offset])

        return {
            steps,
        }
    } finally {
        stream.destroy()
    }
}
