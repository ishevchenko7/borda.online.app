import fs from 'fs'
import path from 'path'
import { promisify } from 'util'

import { replaceAlias } from '#tools/disk'

const open = promisify(fs.open)
const read = promisify(fs.read)
const close = promisify(fs.close)

export default async function (args, dispatcher) {
    let {
        file: { dir, name },
        position,
        length,
    } = args
    const { isAdmin, isAnonymous, currentUrlPath } = dispatcher

    let fullName = path.format({
        dir,
        base: name,
    })
    fullName = isAdmin
        ? fullName
        : await replaceAlias({
              fullName,
              currentUrlPath,
              anonymous: isAnonymous,
          })

    const fd = await open(fullName, 'r')

    try {
        const buffer = Buffer.alloc(length)
        const { bytesRead } = await read(fd, buffer, 0, length, position)

        return {
            bytesRead,
            data: bytesRead < length ? buffer.subarray(0, bytesRead) : buffer,
        }
    } finally {
        await close(fd)
    }
}
