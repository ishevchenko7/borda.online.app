import fs from 'fs'
import path from 'path'
import { promisify } from 'util'

import { replaceAlias, dirSize } from '#tools/disk'

const stat = promisify(fs.stat)

export default async function (args, dispatcher) {
    let {
        file: { dir, name, type },
    } = args
    const { isAdmin, isAnonymous, currentUrlPath } = dispatcher

    let fullName = path.format({
        dir,
        base: name,
    })

    const directFullName = isAdmin
        ? fullName
        : await replaceAlias({
              fullName,
              currentUrlPath,
              anonymous: isAnonymous,
          })

    const size =
        type === 'dir'
            ? await dirSize(directFullName)
            : (await stat(directFullName)).size

    return { name, size }
}
