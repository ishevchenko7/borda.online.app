import path from 'path'
import { replaceAlias, readFilePart } from '#tools/disk'

const maxBufferSize = 1024 * 512

export default async function (args, dispatcher) {
    const {
        file: { dir, name, type },
        position,
    } = args
    const { isAdmin, isAnonymous, currentUrlPath, archiversStore } = dispatcher

    let fullName = path.format({
        dir,
        base: name,
    })

    fullName = isAdmin
        ? fullName
        : await replaceAlias({
              fullName,
              currentUrlPath,
              anonymous: isAnonymous,
          })

    return {
        buffer:
            type === 'dir'
                ? await archiversStore.read({
                      name: fullName,
                      position,
                  })
                : await readFilePart({
                      fullName,
                      position,
                      maxBufferSize,
                  }),
    }
}
