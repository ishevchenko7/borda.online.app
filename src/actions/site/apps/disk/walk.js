import _ from 'lodash'
import { once } from 'events'
import { join, parse } from 'path'
import klaw from 'klaw'

import { replaceAlias, restoreAliasCached } from '#tools/disk'

export default async function (args, dispatcher) {
    const result = {
        dirs: [],
        files: [],
    }
    const { isAdmin, isAnonymous, currentUrlPath } = dispatcher

    const restoreAlias = isAdmin
        ? ({ fullName }) => fullName
        : await restoreAliasCached({ currentUrlPath, anonymous: isAnonymous })

    for (const dirs of args.dirs) {
        const fullName = join(...dirs)

        const directFullName = isAdmin
            ? fullName
            : await replaceAlias({
                  fullName,
                  currentUrlPath,
                  anonymous: isAnonymous,
              })

        const walker = klaw(directFullName).on('data', ({ stats, path }) => {
            const publicPath = restoreAlias({
                fullName: path,
            })

            switch (true) {
                case stats.isDirectory():
                    result.dirs.push(publicPath)
                    break

                case stats.isFile():
                    const { dir, base } = parse(publicPath)
                    result.files.push([dir, base])
                    break
            }
        })

        await once(walker, 'end')
    }

    return result
}
