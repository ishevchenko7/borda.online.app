import _ from 'lodash'
import path from 'path'

import {
    readDir,
    getRoot,
    getSharedRoot,
    replaceAlias,
    isRoot,
} from '#tools/disk'

export default async function (args, dispatcher) {
    const { dirs } = args
    const { isAdmin, isAnonymous, currentUrlPath } = dispatcher

    if (isRoot(dirs)) {
        return isAdmin ? getRoot() : getSharedRoot({ anonymous: isAnonymous })
    } else {
        let dir = path.join(...dirs)
        const items = await readDir(
            isAdmin
                ? dir
                : await replaceAlias({
                      fullName: dir,
                      currentUrlPath,
                      anonymous: isAnonymous,
                  })
        )

        return {
            items,
            dir,
        }
    }
}
