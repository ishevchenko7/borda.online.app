import _ from 'lodash'

import { get, filterWebcams } from '#tools/webcams'

export default async function (args, { isAnonymous, isAdmin }) {
    const webcams = await get()

    if (isAdmin) {
        return webcams
    } else {
        return await filterWebcams({ webcams, anonymous: isAnonymous })
    }
}
