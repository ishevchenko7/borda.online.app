import db from '#db'

export default async function (args, dispatcher) {
    const { url_path } = args
    const { isAdmin, isAnonymous } = dispatcher

    if (url_path) {
        const shared = await db.shared
            .get({
                url_path,
                anonymous: isAnonymous,
                disabled: false,
            })
            .first()

        if (shared) {
            dispatcher.currentUrlPath = url_path
            const { type, name, description, alias, query } = shared
            return {
                shared: {
                    type,
                    name: isAdmin ? name : alias,
                    description,
                    query,
                },
            }
        }
    }
}
