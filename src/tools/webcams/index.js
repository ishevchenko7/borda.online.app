import _ from 'lodash'

import db from '#db'

export async function get() {
    const devices = await navigator.mediaDevices.enumerateDevices()
    const webcams = _.filter(devices, (device) => device.kind === 'videoinput')

    return webcams
}

export async function filterWebcams({ webcams, anonymous }) {
    const filtered = []

    for (const { deviceId, label, ...other } of webcams) {
        const shared = await db.shared
            .get({
                type: 'webcam',
                name: deviceId,
                url_path: null,
                anonymous,
            })
            .first()

        if (shared) {
            filtered.push({
                deviceId: shared.alias,
                label: shared.alias,
                ...other,
            })
        }
    }

    return filtered
}
