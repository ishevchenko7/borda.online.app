import { once } from 'events'
import archiver from 'archiver'

import logger from '#logger'

const maxBufferSize = 1024 * 512

export default class ArchiversStore {
    constructor() {
        this.data = []
        this.log = logger.child({ class: this.constructor.name })
    }

    async read({ name, position }) {
        if (!this.data[name] || position === 0) {
            const stream = archiver('zip', {
                store: true,
            })

            this.data[name] = {
                stream,
            }

            stream.on('error',  (error) => {
                this.log.error({ error, name })
            })

            stream.directory(name, false)
            stream.finalize()
        }

        const datum = this.data[name]
        let { stream, buffer } = datum

        if (position === datum.position) {
            return buffer
        } else {
            if (stream.readable) {
                if (stream.readableLength == 0) {
                    await once(stream, 'readable')
                }

                buffer = await stream.read(
                    stream.readableLength > maxBufferSize
                        ? maxBufferSize
                        : undefined
                )

                _.assign(datum, { position, buffer })
            } else {
                delete this.data[name]
                return
            }

            return buffer
        }
    }
}
