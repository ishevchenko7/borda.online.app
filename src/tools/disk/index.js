import _ from 'lodash'
import fs from 'fs'
import os from 'os'
import path from 'path'
import klaw from 'klaw'
import { promisify } from 'util'
import { once } from 'events'

import childProcess from 'child_process'

import db from '#db'

const open = promisify(fs.open)
const read = promisify(fs.read)
const close = promisify(fs.close)
const readdir = promisify(fs.readdir)
const exec = promisify(childProcess.exec)
const access = promisify(fs.access)
const copyFile = promisify(fs.copyFile)

export async function readDir(dir) {
    let normalDir = path.normalize(dir)
    const items = await readdir(normalDir, { withFileTypes: true })

    const files = []
    const dirs = []

    _.forEach(items, (item) => {
        const { name } = item
        switch (true) {
            case item.isFile():
                files.push(name)
                break
            case item.isDirectory():
                dirs.push(name)
                break
        }
    })

    return { files, dirs }
}

export async function getRoot() {
    switch (os.platform()) {
        case 'linux':
            const root = '/'
            return {
                rootFlag: true,
                items: await readDir(root),
                dir: root,
            }

        case 'win32':
            const { stdout, stderr } = await exec(
                'wmic logicaldisk get name, volumename'
            )

            if (stderr) {
                throw stderr
            }

            const dirs = []
            const volumes = []
            const lines = stdout
                .split('\r\r\n')
                .filter((value) => /[A-Za-z]:/.test(value))

            _.forEach(lines, (line) => {
                const [dir, volume] = line.trim().split(/\s+/)
                dirs.push(dir + '\\')
                volumes.push(volume || '')
            })

            return {
                dir: '',
                rootFlag: true,
                items: {
                    files: [],
                    dirs,
                    volumes: _.isEmpty(volumes) ? undefined : volumes,
                },
            }
    }
}

export async function getSharedRoot({ anonymous }) {
    const filter = {
        url_path: null,
        anonymous,
    }
    const dirs = await db.shared.get({
        type: 'dir',
        ...filter,
    })

    const files = await db.shared.get({
        type: 'file',
        ...filter,
    })

    return {
        dir: '',
        rootFlag: true,
        items: {
            dirs: _.map(dirs, (dir) => dir.alias),
            files: _.map(files, (file) => file.alias),
        },
    }
}

export function isRoot(dirs) {
    const compacted = _.compact(dirs)
    return (
        _.isEmpty(compacted) || path.join(path.sep, ...compacted) === path.sep
    )
}

export async function exists(path) {
    try {
        await access(path)
        return true
    } catch {
        return false
    }
}

export async function copy(source, destination) {
    await copyFile(source, destination)
}

export async function replaceAlias({ fullName, anonymous, currentUrlPath }) {
    const parts = _.split(fullName, path.sep)
    let [alias, ...tail] = parts

    // Fix alias windows special
    if (os.platform() === 'win32' && /^[a-zA-Z]+:$/.test(alias)) {
        alias += '\\'
    }

    const shared = await db.shared
        .getUrlDisabled({
            types: ['dir', 'file'],
            alias,
            currentUrlPath,
            anonymous,
        })
        .first()

    if (shared?.name) {
        return path.join(shared.name, ...tail)
    } else {
        throw Error(`Wrong folder alias: ${alias}`)
    }
}

export async function restoreAliasCached({ currentUrlPath, anonymous }) {
    const shared = await db.shared.getUrlDisabled({
        types: ['dir', 'file'],
        currentUrlPath,
        anonymous,
    })

    return function ({ fullName }) {
        for (const { name, alias } of shared) {
            if (fullName === name) {
                return alias
            } else {
                if (_.startsWith(fullName, path.join(name, path.sep))) {
                    return _.replace(fullName, name, alias)
                }
            }
        }
    }
}

export async function dirSize(directFullName) {
    let size = 0

    const walker = klaw(directFullName).on('data', ({ stats }) => {
        if (stats.isFile()) {
            size += stats.size
        }
    })

    await once(walker, 'end')
    return size
}

export async function readFilePart({ fullName, position, maxBufferSize }) {
    const fd = await open(fullName, 'r')

    try {
        let buffer = Buffer.alloc(maxBufferSize)
        const { bytesRead } = await read(fd, buffer, 0, maxBufferSize, position)

        if (bytesRead > 0) {
            if (bytesRead < maxBufferSize) {
                buffer = buffer.subarray(0, bytesRead)
            }

            return buffer
        }
    } finally {
        await close(fd)
    }
}
