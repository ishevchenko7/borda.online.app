import path from 'path'
import knex from 'knex'
import electron from 'electron'

const userDataPath = electron.remote.app.getPath('userData')

export default knex({
    client: 'sqlite3',
    connection: {
        filename: path.join(userDataPath, 'storage.sqlite'),
    },
    useNullAsDefault: true,
})
