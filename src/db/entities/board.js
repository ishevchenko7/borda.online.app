import knex from '#db/knex'

export async function get() {
    return await knex('board').first()
}

export async function add(board) {
    await knex('board').insert(board)
}
