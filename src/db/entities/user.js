import bcrypt from 'bcryptjs'
import knex from '#db/knex'

export async function get(login) {
    return await knex('users').where({ name: login }).first()
}

export async function add(user) {
    await knex('users').insert(user)
}

export async function remove({ name }) {
    await knex('users').where({ name }).delete()
}

export async function update({ name, user }) {
    await knex('users').where({ name }).update(user)
}

export async function getPartial() {
    return await knex('users').select(
        'name',
        'full_name',
        knex.raw('(hashed_password is not null) as password')
    )
}

export async function setPassword({ name, password }) {
    const hashed_password = await bcrypt.hash(password, 8)
    await knex('users').where({ name }).update({ hashed_password })
}
