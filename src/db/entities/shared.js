import _ from 'lodash'
import knex from '#db/knex'

export async function add(shared) {
    return await knex('shared').insert(shared)
}

export function get(filter) {
    const { types, anonymous, ...other } = filter
    return knex('shared').where((builder) => {
        if (!_.isEmpty(types)) {
            builder.whereIn('type', types)
        }

        builder.where({ ...(anonymous ? { anonymous } : undefined), ...other })
    })
}

export function getUrlDisabled({ currentUrlPath, ...other }) {
    return get(other).where((builder) => {
        builder.where('url_path', null)
        if (currentUrlPath) {
            builder.orWhere('url_path', currentUrlPath)
        }
    })
}

export async function update({ id, value }) {
    return await knex('shared').where({ id }).update(value)
}

export async function remove({ id }) {
    return await knex('shared').where({ id }).delete()
}
