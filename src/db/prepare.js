import { join } from 'path'
import { remote } from 'electron'
import isDev from 'electron-is-dev'

import { exists, copy } from '#tools/disk'

export default async function () {
    const storagePath = join(remote.app.getPath('userData'), 'storage.sqlite')

    if (!(await exists(storagePath))) {
        const source = join(
            remote.app.getAppPath(),
            isDev ? 'src/db' : '../..',
            'storage.sqlite'
        )

        await copy(source, storagePath)
    }
}
